package com.mazzouzi.timelineanimated.ui.theme

import androidx.compose.runtime.Composable
import androidx.compose.runtime.Immutable
import androidx.compose.runtime.Stable
import androidx.compose.runtime.remember
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp

@Composable
fun rememberTimelineState(
    timelineSteps: TimelineSteps,
    timelineTargetStep: TimelineTargetStep,
    currentDensity: TimelineColor,
    stepCircleRadiusDp: Dp,
    halfLineHeightDp: Dp,
    animationDuration: Int,
    animationDelay: Int,
    current: Density
): TimelineState = remember(timelineSteps, timelineTargetStep, currentDensity) {
    TimelineState(
        timelineSteps = timelineSteps,
        timelineTargetStep = timelineTargetStep,
        stepCircleRadiusDp = stepCircleRadiusDp,
        halfLineHeightDp = halfLineHeightDp,
        currentDensity = current,
        timelineColor = currentDensity,
        animationDuration = animationDuration,
        animationDelay = animationDelay,
    )
}

@Stable
class TimelineState(
    timelineSteps: TimelineSteps,
    timelineTargetStep: TimelineTargetStep,
    stepCircleRadiusDp: Dp,
    halfLineHeightDp: Dp,
    currentDensity: Density,
    val timelineColor: TimelineColor,
    val animationDuration: Int,
    val animationDelay: Int,
) {
    val steps: List<Step> = timelineSteps.steps

    val targetStepXOffsetFactor: Float = when (timelineTargetStep) {
        is TimelineTargetStep.ToTargetStep -> steps[
                timelineTargetStep.targetStepIndex.coerceIn(0, steps.size - 1)
        ].xOffset

        TimelineTargetStep.ToTheEnd -> 1f
    }

    val colorTransitionStartXOffsetFactor: Float = when (timelineColor) {
        is TimelineColor.ColorTransition -> {
            steps[timelineColor.stepTransitionStartIndex.coerceIn(0, steps.size - 1)].xOffset
        }

        is TimelineColor.FixedColor -> 0f
    }

    val hasTargetStep = timelineTargetStep is TimelineTargetStep.ToTargetStep

    val stepCircleRadius = with(currentDensity) { stepCircleRadiusDp.toPx() }

    val targetStepInnerCircleRadius = with(currentDensity) { (stepCircleRadiusDp * 0.75f).toPx() }

    val halfLineHeight = with(currentDensity) { halfLineHeightDp.toPx() }

    val timeLineHeightDp = stepCircleRadiusDp * 2
}

sealed interface TimelineColor {

    data class ColorTransition(
        val firstColor: Color,
        val secondColor: Color,
        val stepTransitionStartIndex: Int
    ) : TimelineColor

    data class FixedColor(
        val color: Color
    ) : TimelineColor
}

sealed interface TimelineTargetStep {

    data class ToTargetStep(val targetStepIndex: Int) : TimelineTargetStep

    object ToTheEnd : TimelineTargetStep
}

@Immutable
data class TimelineSteps(val steps: List<Step>)

data class Step(private val xOffsetFactor: Float) {

    val xOffset: Float
        get() = xOffsetFactor.coerceIn(0f, 1f)
}