package com.mazzouzi.timelineanimated

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.geometry.*
import androidx.compose.ui.graphics.ClipOp
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.PathOperation
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.drawscope.clipPath
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.mazzouzi.timelineanimated.ui.theme.*

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            TimelineAnimatedTheme {
                // A surface container using the 'background' color from the theme
                Column(
                    modifier = Modifier.fillMaxSize(),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Timeline(
                        timelineSteps = TimelineSteps(
                            steps = listOf(
                                Step(0.15f),
                                Step(0.57f),
                                Step(0.97f),
                            )
                        ),
                        timelineTargetStep = TimelineTargetStep.ToTheEnd,
                        modifier = Modifier.padding(16.dp),
                        timelineColor = TimelineColor.ColorTransition(
                            firstColor = Color(0xFFFF6E14),
                            secondColor = Color(0xFF4E9850),
                            stepTransitionStartIndex = 1
                        ),
                    )
                }
            }
        }
    }
}

@Composable
fun Timeline(
    timelineSteps: TimelineSteps,
    timelineTargetStep: TimelineTargetStep,
    modifier: Modifier = Modifier,
    timelineColor: TimelineColor = TimelineColor.FixedColor(Color(0xFF4E9850)),
    stepCircleRadiusDp: Dp = 8.dp,
    halfLineHeightDp: Dp = 4.dp,
    animationDuration: Int = ANIMATION_DURATION,
    animationDelay: Int = ANIMATION_DELAY
) {
    val timelineState: TimelineState = rememberTimelineState(
        timelineSteps,
        timelineTargetStep,
        timelineColor,
        stepCircleRadiusDp,
        halfLineHeightDp,
        animationDuration,
        animationDelay,
        LocalDensity.current
    )

    var startAnimation by rememberSaveable {
        mutableStateOf(false)
    }

    val progressFactor by animateFloatAsState(
        targetValue = if (startAnimation) timelineState.targetStepXOffsetFactor else 0f,
        animationSpec = tween(
            durationMillis = timelineState.animationDuration,
            delayMillis = timelineState.animationDelay
        )
    )

    LaunchedEffect(key1 = Unit) {
        startAnimation = true
    }

    val cornerRadius by animateFloatAsState(
        targetValue = if (progressFactor >= timelineState.targetStepXOffsetFactor)
            timelineState.stepCircleRadius
        else
            0f
    )

    val color by when (val colorTransition = timelineState.timelineColor) {
        is TimelineColor.ColorTransition -> {
            animateColorAsState(
                targetValue = if (progressFactor > timelineState.colorTransitionStartXOffsetFactor)
                    colorTransition.secondColor
                else
                    colorTransition.firstColor,
                animationSpec = tween(durationMillis = COLOR_TRANSITION_ANIMATION)
            )
        }
        is TimelineColor.FixedColor -> {
            remember {
                mutableStateOf(colorTransition.color)
            }
        }
    }

    TimeLineCanvas(
        modifier = modifier
            .fillMaxWidth()
            .height(timelineState.timeLineHeightDp)
            .clipToBounds(),
        progressFactor = progressFactor,
        cornerRadius = cornerRadius,
        color = color,
        timelineState = timelineState,
    )
}

@Composable
fun TimeLineCanvas(
    modifier: Modifier = Modifier,
    progressFactor: Float,
    cornerRadius: Float,
    color: Color,
    timelineState: TimelineState,
) {
    Canvas(
        modifier = modifier
    ) {
        val path1 = Path().apply { drawTimeline(this, timelineState.halfLineHeight) }
        val path2 = Path().apply {
            timelineState.steps.forEach { step ->
                drawStepCircle(
                    path = this,
                    xOffSet = size.width
                        .times(step.xOffset)
                        .minus(timelineState.stepCircleRadius),
                    radius = timelineState.stepCircleRadius
                )
            }
        }

        path1.op(path1, path2, PathOperation.Union)

        if (timelineState.hasTargetStep) {
            val path3 = Path().apply {
                drawStepCircle(
                    path = this,
                    xOffSet = size.width
                        .times(timelineState.targetStepXOffsetFactor)
                        .minus(timelineState.stepCircleRadius),
                    radius = timelineState.targetStepInnerCircleRadius
                )
            }

            path1.op(path1, path3, PathOperation.Xor)
        }

        drawPath(path1, Color(0XFFE6EBEF))

        clipPath(
            path = path1,
            clipOp = ClipOp.Intersect
        ) {
            drawRoundRect(
                color = color,
                topLeft = Offset(0f, center.y - timelineState.stepCircleRadius),
                size = Size(size.width * progressFactor, timelineState.stepCircleRadius * 2),
                cornerRadius = CornerRadius(cornerRadius)
            )
        }
    }
}

fun DrawScope.drawTimeline(path: Path, halfLineHeight: Float) {
    path.addRoundRect(
        RoundRect(
            Rect(
                left = 0f,
                top = center.y - halfLineHeight,
                right = size.width,
                bottom = center.y + halfLineHeight,
            ),
            cornerRadius = CornerRadius(halfLineHeight)
        )
    )
}

fun DrawScope.drawStepCircle(path: Path, xOffSet: Float, radius: Float) {
    path.addArc(
        oval = Rect(
            center = Offset(xOffSet, center.y),
            radius = radius
        ),
        startAngleDegrees = 0.0f,
        sweepAngleDegrees = CIRCLE_SWEEP_ANGLE
    )
}

private const val ANIMATION_DURATION = 2000
private const val ANIMATION_DELAY = 500
private const val CIRCLE_SWEEP_ANGLE = 360f
private const val COLOR_TRANSITION_ANIMATION = 1000

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    TimelineAnimatedTheme {
        Timeline(
            timelineSteps = TimelineSteps(
                steps = listOf(
                    Step(0.15f),
                    Step(0.57f),
                    Step(0.97f),
                )
            ),
            timelineTargetStep = TimelineTargetStep.ToTargetStep(2),
            modifier = Modifier.padding(16.dp),
            timelineColor = TimelineColor.ColorTransition(
                firstColor = Color(0xFFFF6E14),
                secondColor = Color(0xFF4E9850),
                stepTransitionStartIndex = 1
            ),
        )
    }
}